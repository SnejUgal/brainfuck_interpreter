use std::future::Future;

mod interpreter;
mod parser;

pub use interpreter::{EvaluationError, State};
pub use parser::{ParseError, ParseErrorKind};

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
enum Instruction {
    IncrementValue,
    DecrementValue,
    IncrementPointer,
    DecrementPointer,
    PrintValue,
    ReadValue,
    Loop(Instructions),
}

type Instructions = Vec<Instruction>;

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Program(Instructions);

impl Program {
    pub fn parse(code: &str) -> Result<Self, ParseError> {
        parser::parse(code).map(Self)
    }

    pub async fn run<R, RF, P, PF>(&self, read: R, print: P) -> Result<State, EvaluationError>
    where
        R: (FnMut() -> RF) + Send,
        RF: Future<Output = u8> + Send,
        P: (FnMut(u8) -> PF) + Send,
        PF: Future<Output = ()> + Send,
    {
        interpreter::interpret(&self.0, read, print).await
    }
}
