use super::{Instruction, Instructions};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct ParseError {
    pub kind: ParseErrorKind,
    pub offset: usize,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum ParseErrorKind {
    UnexpectedLoopEnd,
    ExpectedLoopEnd,
}

fn parse_chars(
    characters: impl Iterator<Item = char>,
    offset: usize,
) -> Result<Instructions, ParseError> {
    let mut program = Vec::new();

    let mut loop_body: Option<String> = None;
    let mut nesting_level = 0;
    let mut length = offset;

    for (index, character) in characters.enumerate() {
        length += 1;

        if let Some(mut body) = loop_body {
            match character {
                '+' | '-' | '<' | '>' | '.' | ',' => body.push(character),
                '[' => {
                    body.push('[');
                    nesting_level += 1;
                }
                ']' => {
                    nesting_level -= 1;
                    if nesting_level == 0 {
                        let parsed_loop = parse_chars(body.chars(), offset)?;
                        program.push(Instruction::Loop(parsed_loop));
                        loop_body = None;
                        continue;
                    } else {
                        body.push(']');
                    }
                }
                _ => (),
            }
            loop_body = Some(body);
            continue;
        }

        match character {
            '+' => program.push(Instruction::IncrementValue),
            '-' => program.push(Instruction::DecrementValue),
            '>' => program.push(Instruction::IncrementPointer),
            '<' => program.push(Instruction::DecrementPointer),
            '.' => program.push(Instruction::PrintValue),
            ',' => program.push(Instruction::ReadValue),
            '[' => {
                loop_body = Some(String::new());
                nesting_level = 1;
            }
            ']' => {
                return Err(ParseError {
                    kind: ParseErrorKind::UnexpectedLoopEnd,
                    offset: index + offset,
                });
            }
            _ => (),
        }
    }

    if loop_body.is_none() {
        Ok(program)
    } else {
        Err(ParseError {
            kind: ParseErrorKind::ExpectedLoopEnd,
            offset: length,
        })
    }
}

pub(crate) fn parse(code: &str) -> Result<Instructions, ParseError> {
    parse_chars(code.chars(), 0)
}
