use crate::{Instruction, Instructions};
use std::{future::Future, pin::Pin};

type Cell = u8;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum EvaluationError {
    PointerOverflow,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct State {
    cells: Vec<Cell>,
    pointer: usize,
}

impl State {
    fn increment_pointer(&mut self) -> Result<(), EvaluationError> {
        self.pointer = self
            .pointer
            .checked_add(1)
            .ok_or(EvaluationError::PointerOverflow)?;

        Ok(())
    }

    fn decrement_pointer(&mut self) -> Result<(), EvaluationError> {
        self.pointer = self
            .pointer
            .checked_sub(1)
            .ok_or(EvaluationError::PointerOverflow)?;

        Ok(())
    }

    fn resize_if_necessary(&mut self) {
        if self.pointer < self.cells.len() {
            return;
        }

        self.cells.resize(self.pointer + 1, 0);
    }

    fn increment_value(&mut self) {
        self.resize_if_necessary();
        self.cells[self.pointer] = self.cells[self.pointer].wrapping_add(1);
    }

    fn decrement_value(&mut self) {
        self.resize_if_necessary();
        self.cells[self.pointer] = self.cells[self.pointer].wrapping_sub(1);
    }

    fn write(&mut self, value: Cell) {
        self.resize_if_necessary();
        self.cells[self.pointer] = value;
    }

    fn read(&mut self) -> Cell {
        self.resize_if_necessary();
        self.cells[self.pointer]
    }
}

fn run_instructions<'a, R, RF, P, PF>(
    state: &'a mut State,
    instructions: &'a Instructions,
    read: &'a mut R,
    print: &'a mut P,
) -> Pin<Box<dyn Future<Output = Result<(), EvaluationError>> + Send + 'a>>
where
    R: (FnMut() -> RF) + Send + 'a,
    RF: Future<Output = Cell> + Send,
    P: (FnMut(Cell) -> PF) + Send + 'a,
    PF: Future<Output = ()> + Send,
{
    Box::pin(async move {
        for instruction in instructions {
            match instruction {
                Instruction::IncrementValue => state.increment_value(),
                Instruction::DecrementValue => state.decrement_value(),
                Instruction::IncrementPointer => state.increment_pointer()?,
                Instruction::DecrementPointer => state.decrement_pointer()?,
                Instruction::Loop(loop_body) => {
                    while state.read() != 0 {
                        run_instructions(state, loop_body, read, print).await?
                    }
                }
                Instruction::PrintValue => print(state.read()).await,
                Instruction::ReadValue => state.write(read().await),
            }
        }

        Ok(())
    })
}

pub(crate) async fn interpret<R, RF, P, PF>(
    instructions: &Instructions,
    mut read: R,
    mut print: P,
) -> Result<State, EvaluationError>
where
    R: (FnMut() -> RF) + Send,
    RF: Future<Output = Cell> + Send,
    P: (FnMut(Cell) -> PF) + Send,
    PF: Future<Output = ()> + Send,
{
    let mut state = State {
        cells: Vec::new(),
        pointer: 0,
    };

    run_instructions(&mut state, instructions, &mut read, &mut print).await?;

    Ok(state)
}
