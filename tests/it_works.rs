use brainfuck_interpreter::Program;

const PROGRAM: &str = ",>,[-<+>]<.";

const INPUT: &[u8] = &[42, 69];
const OUTPUT: &[u8] = &[42 + 69];

#[tokio::test]
async fn main() {
    let program = Program::parse(PROGRAM).unwrap();

    let mut input = INPUT.iter();
    let read = move || {
        let return_value = input.next().copied().unwrap_or(0);
        async move { return_value }
    };

    let mut output = OUTPUT.iter();
    let print = move |value| {
        assert_eq!(Some(value), output.next().copied());
        async {}
    };

    program.run(read, print).await.unwrap();
}
