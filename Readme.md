# `brainfuck_interpreter`

A Brainfuck interpreter in Rust which allows custom asynchronous I/O.

Unlike many other interpreters, this interpreter does not do any I/O on `.`
and `,` instructions on itself, but instead calls the corresponding handlers
provided by you. This lets you do custom I/O in Brainfuck programs,
e.g. you can write a quest where one needs to write a Brainfuck program,
or you can write a TCP server in Brainfuck and so on. The interpeter also lets
the handlers be asynchronous, and it is also runtime-agnostic.
